#!/bin/bash
echo "10.10.10.10 freeipa.mydomain.xyz freeipa" >> /etc/hosts
firewall-cmd --set-default-zone=public
firewall-cmd --add-service=freeipa-ldap --add-service=freeipa-ldaps --zone=public
firewall-cmd --add-service=freeipa-ldap --add-service=freeipa-ldaps --permanent --zone=public
firewall-cmd --add-lockdown-whitelist-command=$(which firewall-cmd)
firewall-cmd --lockdown-on
firewall-cmd --runtime-to-permanent
firewall-cmd --complete-reload
echo "to install run ipa-server-install"
echo "to add internal dns services run ipa-dns-install after primary install done"
