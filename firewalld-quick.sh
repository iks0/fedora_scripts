#!/bin/bash
# run as root at first boot, preferably before your first dnf update
# paranoid firewall-cmd quick setup for new hosts
firewall-cmd --set-default-zone=drop
firewall-cmd --add-lockdown-whitelist-command=$(which firewall-cmd)
firewall-cmd --lockdown-on
firewall-cmd --runtime-to-permanent
firewall-cmd --complete-reload
